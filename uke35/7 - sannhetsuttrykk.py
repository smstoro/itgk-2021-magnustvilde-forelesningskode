'''sannhetsuttrykk'''

# ta inn navnet til brukeren, og sett en variabel til True dersom navnet
# er 'Magnus'. False utenom. Skriv ut resultatet.
navn = input('Skriv navnet ditt: ')
sjekk = navn == 'Magnus'
print(sjekk)

# ta inn et tall og sjekk om tallet delt på 3 er større enn 14. Skriv ut resultatet
tall = int(input('Tall: '))
storreEnn14 = (tall/3) > 14
print(storreEnn14)


